package view;

import controller.Partida;
import model.Puntuacion;

import java.util.Scanner;

public class Menu {

    public Menu() {
        Puntuacion puntuacion = new Puntuacion();
        Scanner scanner = new Scanner(System.in);
        int opcion;

        do{
            System.out.println("TRES EN RAYA");
            System.out.println("1. Jugar partida");
            System.out.println("2. Consultar puntuaciones");
            System.out.println("3. Resetear puntuaciones");
            System.out.println("0. Salir");
            System.out.println("Opción: ");

            while (!scanner.hasNextInt()){
                System.out.print("Selecciona una opción correcta.\n");
                scanner.nextLine();
            }

            opcion=scanner.nextInt();
            scanner.nextLine();

            switch (opcion) {
                case 1:
                    new Partida(puntuacion);
                    break;
                case 2:
                    System.out.println("Partidas ganadas X: " + puntuacion.getGanaX());
                    System.out.println("Partidas ganadas O: " + puntuacion.getGanaO() + "\n");
                    break;
                case 3:
                    System.out.println("Introduce password administrador: ");
                    String pwd = scanner.next();
                    if (pwd.equals("admin")){
                        puntuacion.reset();
                        System.out.println("Puntuaciones reseteadas.\n");
                    }
                    else {
                        System.out.println("Password incorrecto.\n");
                    }
                    break;
            }
        }while (opcion != 0);
    }
}
