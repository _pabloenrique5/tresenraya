package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PuntuacionTest {

    Puntuacion puntuacion;

    @BeforeEach
    void setUp() {
        puntuacion = new Puntuacion();
    }


    @Test
    void ganaX() {
        int oldX = puntuacion.getGanaX();
        puntuacion.ganaX();
        assertEquals(oldX+1, puntuacion.getGanaX());
    }

    @Test
    void ganaO() {
        int oldO = puntuacion.getGanaO();
        puntuacion.ganaO();
        assertEquals(oldO+1, puntuacion.getGanaO());
    }

    @Test
    void reset() {
        puntuacion.reset();
        assertEquals(0, puntuacion.getGanaO());
        assertEquals(0, puntuacion.getGanaX());
    }
}