package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TurnoTest {

    Turno turno;

    @BeforeEach
    void setUp() {
        turno = new Turno(Ficha.X);
    }

    @Test
    void cambiarTurno() {
        turno.cambiarTurno();
        assertEquals(Ficha.O, turno.getFicha());
    }

}