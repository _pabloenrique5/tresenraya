package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TableroTest {

    Tablero tablero;

    @BeforeEach
    void setUp() {
        tablero = new Tablero();
    }

    @Test
    void getPosicion() {
        assertEquals("8", tablero.getPosicion(8));
    }

    @Test
    void ponerFicha() {
        tablero.ponerFicha(5, Ficha.X);
        assertEquals(Ficha.X.name(), "X");
    }

    @Test
    void posicionLibre() {
        assertEquals(true, tablero.posicionLibre(4));
    }

    @Test
    void estaLleno() {
        assertFalse(tablero.estaLleno());
    }

    @Test
    void hayGanador() {
        assertFalse(tablero.hayGanador());
    }
}